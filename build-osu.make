api = 2
core = 7.x

projects[] = drupal

projects[osu][type] = profile
projects[osu][download][type] = git
projects[osu][download][url] = https://kautto@bitbucket.org/kautto/osu-lite.git
projects[osu][download][branch] = 7.x-3.x
