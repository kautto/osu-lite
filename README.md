Open OSU Install Profile
------------------------

This Drupal installation profile leverages work from:

* [Wetkit](https://drupal.org/project/wetkit) 
* [Panopoly](https://drupal.org/project/panopoly)

along with dozens of OSU specific modules making up the OpenOSU distribution.

## Building a Platform

To build a platform, use the [drush](https://github.com/drush-ops/drush) 
command [make](http://www.webomelette.com/how-use-drush-make-your-daily-routine).

```
git clone https://code.osu.edu/openosu/osu_profile.git
drush make osu_profile/build-osu.make your-platform
```
