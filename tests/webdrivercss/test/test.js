
var website_url = 'http://localhost:8080/demo';

var webdriverjs = require('webdriverjs'),
    assert      = require('assert');


describe('my website should always look the same',function() {

	this.timeout(99999999);
    var client = {};

    before(function(){
        client = webdriverjs.remote({
            desiredCapabilities: {
                browserName: 'chrome',
                version: '34',
                platform: 'OS X 10.9',
                tags: ['travis-ci'],
                name: 'Engineering platform build',
                'tunnel-identifier': process.env.TRAVIS_JOB_NUMBER
            },
            host: 'localhost',
            port: 4445,
            user: process.env.SAUCE_USERNAME,
            key: process.env.SAUCE_ACCESS_KEY
            // logLevel: 'silent'
        });

        // initialise WebdriverCSS for `client` instance
		require('webdrivercss').init(client, {
		    screenshotRoot: 'screenshots',
		    failedComparisonsRoot: 'diffs',
		    misMatchTolerance: 0.05,
		    screenWidth: [320,480,640,1024]
		});

		client.init();
    });

    it('header should look the same',function(done) {
        client
            .url(website_url)
            .webdrivercss('header', {
             	elem: '.l-header-outer',
            }, function(err,res) {
                assert.equal(err, null);

                // this will break the test if screenshot differ more then 5% from
                // the previous taken image
                console.log(res.message);
                assert.equal(res.misMatchPercentage < 5, true);
            })
            .call(done);

    });

    after(function(done) {
    	client.end(done);
    });

});
