var phantomcss = require('../node_modules/phantomcss/phantomcss.js');
var fs = require('fs');

phantomcss.init({
	// screenshotRoot: '/screenshots',
	// failedComparisonsRoot: '/failures'
	// casper: specific_instance_of_casper,
	libraryRoot: fs.absolute('node_modules/phantomcss'),

  // Mismatch tolerance defaults to .07%. Increasing this value will decrease test coverage
  mismatchTolerance: 0.07,

	// fileNameGetter: function overide_file_naming(){},
	// onPass: function passCallback(){},
	// onFail: function failCallback(){},
	// onTimeout: function timeoutCallback(){},
	// onComplete: function completeCallback(){},
	// hideElements: '#thing.selector',
	// addLabelToFailedImage: true,
	// outputSettings: {
	// 	errorColor: {
	// 		red: 255,
	// 		green: 255,
	// 		blue: 0
	// 	},
	// 	errorType: 'movement',
	// 	transparency: 0.3
	// }
});

/*
	The test scenario
*/
casper.start('http://127.0.0.1:8080');

viewports = [
  {
    'name': 'samsung-galaxy_y-portrait',
    'viewport': {width: 240, height: 320}
  },
  {
    'name': 'samsung-galaxy_y-landscape',
    'viewport': {width: 320, height: 240}
  },
  {
    'name': 'iphone5-portrait',
    'viewport': {width: 320, height: 568}
  },
  {
    'name': 'iphone5-landscape',
    'viewport': {width: 568, height: 320}
  },
  {
    'name': 'htc-one-portrait',
    'viewport': {width: 360, height: 640}
  },
  {
    'name': 'htc-one-landscape',
    'viewport': {width: 640, height: 360}
  },
  {
    'name': 'nokia-lumia-920-portrait',
    'viewport': {width: 240, height: 320}
  },
  {
    'name': 'nokia-lumia-920-landscape',
    'viewport': {width: 320, height: 240}
  },
  {
    'name': 'google-nexus-7-portrait',
    'viewport': {width: 603, height: 966}
  },
  {
    'name': 'google-nexus-7-landscape',
    'viewport': {width: 966, height: 603}
  },
  {
    'name': 'ipad-portrait',
    'viewport': {width: 768, height: 1024}
  },
  {
    'name': 'ipad-landscape',
    'viewport': {width: 1024, height: 768}
  },
  {
    'name': 'desktop-standard-vga',
    'viewport': {width: 640, height: 480}
  },
  {
    'name': 'desktop-standard-svga',
    'viewport': {width: 800, height: 600}
  },
  {
    'name': 'desktop-standard-hd',
    'viewport': {width: 1280, height: 720}
  },
  // {
  //   'name': 'desktop-standard-sxga',
  //   'viewport': {width: 1280, height: 1024}
  // },
  // {
  //   'name': 'desktop-standard-sxga-plus',
  //   'viewport': {width: 1400, height: 1050}
  // },
  // {
  //   'name': 'desktop-standard-uxga',
  //   'viewport': {width: 1600, height: 1200}
  // },
  // {
  //   'name': 'desktop-standard-wuxga',
  //   'viewport': {width: 1920, height: 1200}
  // },

];

casper.each(viewports, function(casper, viewport) {
	this.then(function() {
    	this.viewport(viewport.viewport.width, viewport.viewport.height);
  	});

	this.then(function(){
		phantomcss.screenshot('.l-header-outer', 'Header area ' + viewport.name);
	});

	// casper.then(function(){
	// 	phantomcss.screenshot('.l-footer-outer', 'Footer area');
	// });
});


casper.then( function now_check_the_screenshots(){
	// compare screenshots
	phantomcss.compareAll();
});

casper.then( function end_it(){
	casper.test.done();
});

/*
Casper runs tests
*/
casper.run(function(){
	console.log('\nTHE END.');
	phantom.exit(phantomcss.getExitStatus());
});

