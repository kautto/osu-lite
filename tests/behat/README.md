Behat tests
===========

Setup
-----

 1. Install Composer

    https://getcomposer.org/doc/00-intro.md#globally
 
 2. Install Behat and dependencies via Composer

    composer install

 3. Copy behat.example.yml to behat.yml and modify

    mv behat.example.yml behat.yml

 4. Get selenium server up and running

    http://docs.seleniumhq.org/download/
    java -jar selenium-server-standalone-2.39.0.jar
 
 5. Run Behat and examine test results!
 
    bin/behat

