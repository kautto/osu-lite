Feature: Add map widget
  In order to put a map on a page
  As a site administrator
  I need to be able to use the map widget

  @api @javascript
  Scenario: Add map to a page
    Given I am logged in as a user with the "administrator" role
    And Panopoly magic live previews are disabled
    When I visit "/node/add/osu-landing-page"
    And I fill in the following:
      | Title               | Testing title |
    And I press "Save"
    Then I should see "Landing Page Testing title has been created."
    When I customize this page with the Panels IPE
      And I click "Add new pane"
      And I click "Add map"
    Then I should see "Configure new Add map"
    When I fill in the following:
      | Title       | Widget title            |
      | Address     | Ørnebjergvej 28, Vejle  |
      And the iframe in element "cke_1_contents" has id "cke_main_body_ifr"
      And I fill in "Testing text body field" in WYSIWYG editor "cke_main_body_ifr"
      And I press "edit-return"
      And I press "Save as custom"
      And I wait for the Panels IPE to deactivate
    Then I should see "Widget title"
      And I should see "Testing text body field"
