Feature: Add a page
  In order to create a page
  As a site administrator
  I need to be able to create a page

  @api @javascript
  Scenario: Add a page
    Given I am logged in as a user with the "administrator" role
    When I visit "/node/add/osu-page"
    And I fill in the following:
      | Title            | Testing 123  |
      | menu[link_title] | My Menu Title |
    And the iframe in element "cke_1_contents" has id "cke_main_body_ifr"
    And I fill in "Testing Page" in WYSIWYG editor "cke_main_body_ifr"
    And I press "Save"
    Then I should see "Page Testing 123 has been created."
