Feature: Add landing page
  In order to create a fully customizable page
  As a site administrator
  I need to be able to create a landing page

  # @javascript is needed for the machine name
  @api @javascript
  Scenario: Add a landing page
    Given I am logged in as a user with the "administrator" role
    When I visit "/node/add/osu-landing-page"
    And I fill in the following:
      | Title               | Testing title |
    And I press "Save"
    Then I should see "Landing Page Testing title has been created."
