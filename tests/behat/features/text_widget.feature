Feature: Add text widget
  In order to put additional text on a page (beyond the main content)
  As a site administrator
  I need to be able to add a text widget
 
  @api @javascript
  Scenario: Add text to a page
    Given I am logged in as a user with the "administrator" role
      And Panopoly magic live previews are disabled
    When I visit "/node/add/osu-landing-page"
      And I fill in the following:
      | Title               | Testing title |
      And I press "Save"
    Then I should see "Landing Page Testing title has been created."
    When I customize this page with the Panels IPE
      And I click "Add new pane"
      And I click "Add text"
    Then I should see "Configure new Add text"
    When I fill in the following:
      | Title   | Text widget title       |
      And the iframe in element "cke_1_contents" has id "cke_main_body_ifr"
      And I fill in "Testing text body field" in WYSIWYG editor "cke_main_body_ifr"
      And I press "edit-return"
      And I press "Save as custom"
      And I wait for the Panels IPE to deactivate
    Then I should see "Text widget title"
      And I should see "Testing text body field"
