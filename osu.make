api = 2
core = 7.x

; The Panopoly Foundation

projects[panopoly_core][version] = 1.18
projects[panopoly_core][subdir] = panopoly

projects[panopoly_theme][version] = 1.18
projects[panopoly_theme][subdir] = panopoly

projects[panopoly_magic][version] = 1.18
projects[panopoly_magic][subdir] = panopoly

projects[panopoly_widgets][version] = 1.18
projects[panopoly_widgets][subdir] = panopoly

projects[panopoly_admin][version] = 1.18
projects[panopoly_admin][subdir] = panopoly

projects[panopoly_images][version] = 1.18
projects[panopoly_images][subdir] = panopoly
projects[panopoly_images][patch][1111111] = https://raw.githubusercontent.com/osu-eng/osu_patches/master/panopoly_images-fix-view-modes.patch

; The Panopoly Toolset

; projects[panopoly_search][version] = 1.18
; projects[panopoly_search][subdir] = panopoly

; OSU Features

;projects[km][type] = module
;projects[km][subdir] = custom
;projects[km][download][type] = git
;projects[km][download][url] = https://code.osu.edu/kmdata/km.git
;projects[km][download][branch] = 7.x-1.x

;projects[osu_widgets][type] = module
;projects[osu_widgets][subdir] = custom
;projects[osu_widgets][download][type] = git
;projects[osu_widgets][download][url] = https://code.osu.edu/openosu/osu_widgets.git
;projects[osu_widgets][download][branch] = 7.x-1.x

projects[osu_admin][type] = module
projects[osu_admin][subdir] = custom
projects[osu_admin][download][type] = git
projects[osu_admin][download][url] = https://code.osu.edu/openosu/osu_admin.git
projects[osu_admin][download][branch] = 7.x-1.x

;projects[osu_categories][type] = module
;projects[osu_categories][subdir] = custom
;projects[osu_categories][download][type] = git
;projects[osu_categories][download][url] = https://code.osu.edu/openosu/osu_categories.git
;projects[osu_categories][download][branch] = 7.x-1.x

projects[osu_devel][type] = module
projects[osu_devel][subdir] = custom
projects[osu_devel][download][type] = git
projects[osu_devel][download][url] = https://code.osu.edu/openosu/osu_devel.git
projects[osu_devel][download][branch] = 7.x-1.x

;projects[osu_linkchecker][type] = module
;projects[osu_linkchecker][subdir] = custom
;projects[osu_linkchecker][download][type] = git
;projects[osu_linkchecker][download][url] = https://code.osu.edu/openosu/osu_linkchecker.git
;projects[osu_linkchecker][download][branch] = 7.x-1.x

projects[osu_menus][type] = module
projects[osu_menus][subdir] = custom
projects[osu_menus][download][type] = git
projects[osu_menus][download][url] = https://code.osu.edu/openosu/osu_menus.git
projects[osu_menus][download][branch] = 7.x-1.x

projects[osu_core][type] = module
projects[osu_core][subdir] = custom
projects[osu_core][download][type] = git
projects[osu_core][download][url] = https://code.osu.edu/openosu/osu_core.git
projects[osu_core][download][branch] = 7.x-1.x

projects[osu_wysiwyg][type] = module
projects[osu_wysiwyg][subdir] = custom
projects[osu_wysiwyg][download][type] = git
projects[osu_wysiwyg][download][url] = https://code.osu.edu/openosu/osu_wysiwyg.git
projects[osu_wysiwyg][download][branch] = 7.x-2.x

projects[osu_text_format][type] = module
projects[osu_text_format][subdir] = custom
projects[osu_text_format][download][type] = git
projects[osu_text_format][download][url] = https://code.osu.edu/openosu/osu_text_format.git
projects[osu_text_format][download][branch] = 7.x-2.x

projects[osu_codemirror][type] = module
projects[osu_codemirror][subdir] = custom
projects[osu_codemirror][download][type] = git
projects[osu_codemirror][download][url] = https://code.osu.edu/openosu/osu_codemirror.git
projects[osu_codemirror][download][branch] = 7.x-1.x

;projects[osu_moderation][type] = module
;projects[osu_moderation][subdir] = custom
;projects[osu_moderation][download][type] = git
;projects[osu_moderation][download][url] = https://code.osu.edu/openosu/osu_moderation.git
;projects[osu_moderation][download][branch] = 7.x-1.x

projects[osu_siteinfo][type] = module
projects[osu_siteinfo][subdir] = custom
projects[osu_siteinfo][download][type] = git
projects[osu_siteinfo][download][url] = https://kautto@bitbucket.org/kautto/osu-siteinfo.git
projects[osu_siteinfo][download][branch] = 7.x-3.x

projects[osu_images][type] = module
projects[osu_images][subdir] = custom
projects[osu_images][download][type] = git
projects[osu_images][download][url] = https://code.osu.edu/openosu/osu_images.git
projects[osu_images][download][branch] = 7.x-1.x

projects[osu_media][type] = module
projects[osu_media][subdir] = custom
projects[osu_media][download][type] = git
projects[osu_media][download][url] = https://code.osu.edu/openosu/osu_media.git
projects[osu_media][download][branch] = 7.x-1.x

projects[osu_theme][type] = module
projects[osu_theme][subdir] = custom
projects[osu_theme][download][type] = git
projects[osu_theme][download][url] = https://kautto@bitbucket.org/kautto/osu-theme.git
projects[osu_theme][download][branch] = 7.x-3.x

projects[osu_authentication][type] = module
projects[osu_authentication][subdir] = custom
projects[osu_authentication][download][type] = git
projects[osu_authentication][download][url] = https://code.osu.edu/openosu/osu_authentication.git
projects[osu_authentication][download][branch] = 7.x-1.x

;projects[osu_lockdown][type] = module
;projects[osu_lockdown][subdir] = custom
;projects[osu_lockdown][download][type] = git
;projects[osu_lockdown][download][url] = https://code.osu.edu/openosu/osu_lockdown.git
;projects[osu_lockdown][download][branch] = 7.x-1.x

;projects[osu_help][type] = module
;projects[osu_help][subdir] = custom
;projects[osu_help][download][type] = git
;projects[osu_help][download][url] = https://code.osu.edu/openosu/osu_help.git
;projects[osu_help][download][branch] = 7.x-1.x

;projects[osu_twitter][type] = module
;projects[osu_twitter][subdir] = custom
;projects[osu_twitter][download][type] = git
;projects[osu_twitter][download][url] = https://code.osu.edu/openosu/osu_twitter.git
;projects[osu_twitter][download][branch] = 7.x-1.x

;projects[osu_analytics][type] = module
;projects[osu_analytics][subdir] = custom
;projects[osu_analytics][download][type] = git
;projects[osu_analytics][download][url] = https://code.osu.edu/openosu/osu_analytics.git
;projects[osu_analytics][download][branch] = 7.x-1.x

;projects[osu_ws][type] = module
;projects[osu_ws][subdir] = custom
;projects[osu_ws][download][type] = git
;projects[osu_ws][download][url] = https://code.osu.edu/openosu/osu_ws.git
;projects[osu_ws][download][branch] = 7.x-1.x

;projects[osu_seo][type] = module
;projects[osu_seo][subdir] = custom
;projects[osu_seo][download][type] = git
;projects[osu_seo][download][url] = https://code.osu.edu/openosu/osu_seo.git
;projects[osu_seo][download][branch] = 7.x-1.x

projects[osu_js_injector][type] = module
projects[osu_js_injector][subdir] = custom
projects[osu_js_injector][download][type] = git
projects[osu_js_injector][download][url] = https://code.osu.edu/openosu/osu_js_injector.git
projects[osu_js_injector][download][branch] = 7.x-1.x

;projects[osu_logs][type] = module
;projects[osu_logs][subdir] = custom
;projects[osu_logs][download][type] = git
;projects[osu_logs][download][url] = https://code.osu.edu/openosu/osu_logs.git
;projects[osu_logs][download][branch] = 7.x-1.x

; OSU Content Types

; Base Fields
projects[osu_fields][type] = module
projects[osu_fields][subdir] = custom
projects[osu_fields][download][type] = git
projects[osu_fields][download][url] = https://code.osu.edu/openosu/osu_fields.git
projects[osu_fields][download][branch] = 7.x-1.x

; Authors
;projects[osu_author][type] = module
;projects[osu_author][subdir] = custom
;projects[osu_author][download][type] = git
;projects[osu_author][download][url] = https://code.osu.edu/openosu/osu_author.git
;projects[osu_author][download][branch] = 7.x-1.x

; Blogs
;projects[osu_blog][type] = module
;projects[osu_blog][subdir] = custom
;projects[osu_blog][download][type] = git
;projects[osu_blog][download][url] = https://code.osu.edu/openosu/osu_blog.git
;projects[osu_blog][download][branch] = 7.x-1.x

; Courses
;projects[osu_courses][type] = module
;projects[osu_courses][subdir] = custom
;projects[osu_courses][download][type] = git
;projects[osu_courses][download][url] = https://code.osu.edu/openosu/osu_courses.git
;projects[osu_courses][download][branch] = 7.x-1.x

; Events
;projects[osu_events][type] = module
;projects[osu_events][subdir] = custom
;projects[osu_events][download][type] = git
;projects[osu_events][download][url] = https://code.osu.edu/openosu/osu_events.git
;projects[osu_events][download][branch] = 7.x-1.x

; News
;projects[osu_news][type] = module
;projects[osu_news][subdir] = custom
;projects[osu_news][download][type] = git
;projects[osu_news][download][url] = https://code.osu.edu/openosu/osu_news.git
;projects[osu_news][download][branch] = 7.x-1.x

; Pages
projects[osu_pages][type] = module
projects[osu_pages][subdir] = custom
projects[osu_pages][download][type] = git
projects[osu_pages][download][url] = https://code.osu.edu/openosu/osu_pages.git
projects[osu_pages][download][branch] = 7.x-1.x

; People
;projects[osu_people][type] = module
;projects[osu_people][subdir] = custom
;projects[osu_people][download][type] = git
;projects[osu_people][download][url] = https://code.osu.edu/openosu/osu_people.git
;projects[osu_people][download][branch] = 7.x-1.x

; Courses
;projects[osu_courses][type] = module
;projects[osu_courses][subdir] = custom
;projects[osu_courses][download][type] = git
;projects[osu_courses][download][url] = https://code.osu.edu/openosu/osu_courses.git
;projects[osu_courses][download][branch] = 7.x-1.x

; Landing page
projects[osu_landing_page][type] = module
projects[osu_landing_page][subdir] = custom
projects[osu_landing_page][download][type] = git
projects[osu_landing_page][download][url] = https://code.osu.edu/openosu/osu_landing_pages.git
projects[osu_landing_page][download][branch] = 7.x-1.x

; Student orgs
;projects[osu_student_orgs][type] = module
;projects[osu_student_orgs][subdir] = custom
;projects[osu_student_orgs][download][type] = git
;projects[osu_student_orgs][download][url] = https://code.osu.edu/openosu/osu_student_orgs.git
;projects[osu_student_orgs][download][branch] = 7.x-1.x

; Reports
;projects[osu_reports][type] = module
;projects[osu_reports][subdir] = custom
;projects[osu_reports][download][type] = git
;projects[osu_reports][download][url] = https://code.osu.edu/openosu/osu_reports.git
;projects[osu_reports][download][branch] = 7.x-1.x

;projects[health_restws][type] = module
;projects[health_restws][subdir] = custom
;projects[health_restws][download][type] = git
;projects[health_restws][download][url] = http://git.drupal.org/sandbox/jasonlttl/2364363.git
;projects[health_restws][download][branch] = 7.x-1.x

; RSS Feeds
;projects[osu_rss][type] = module
;projects[osu_rss][subdir] = custom
;projects[osu_rss][download][type] = git
;projects[osu_rss][download][url] = https://code.osu.edu/openosu/osu_rss.git
;projects[osu_rss][download][branch] = 7.x-1.x


; Engineering Specific Modules

projects[eng_config][type] = module
projects[eng_config][subdir] = custom
projects[eng_config][download][type] = git
projects[eng_config][download][url] = https://code.osu.edu/ews/eng_config.git
projects[eng_config][download][branch] = 7.x-1.x

;projects[eng2osu][type] = module
;projects[eng2osu][subdir] = custom
;projects[eng2osu][download][type] = git
;projects[eng2osu][download][url] = https://code.osu.edu/ews/eng2osu.git
;projects[eng2osu][download][branch] = 7.x-1.x

projects[redirect_404][type] = module
projects[redirect_404][subdir] = custom
projects[redirect_404][download][type] = git
projects[redirect_404][download][url] = https://code.osu.edu/ews/redirect_404.git
projects[redirect_404][download][branch] = 7.x-1.x

;projects[csm][type] = module
;projects[csm][subdir] = custom
;projects[csm][download][type] = git
;projects[csm][download][url] = https://code.osu.edu/openosu/csm.git
;projects[csm][download][branch] = 7.x-1.x

; Field Collection
projects[field_collection][version] = 1.x-dev
projects[field_collection][subdir] = contrib
projects[field_collection][download][type] = git
projects[field_collection][download][revision] = 0fd332e
projects[field_collection][download][branch] = 7.x-1.x
projects[field_collection][patch][707484] = http://drupal.org/files/field_collection-remove_button-707484-2.patch
projects[field_collection][patch][2075325] = http://drupal.org/files/issues/field_collection-field_collection_uuid-2075325-3.patch
projects[field_collection][patch][2000690] = http://drupal.org/files/2000690-field_collection-node-revision-deletion-issues-3.patch


; Themes

projects[osu_shiny][type] = theme
projects[osu_shiny][download][type] = git
projects[osu_shiny][download][url] = https://code.osu.edu/openosu/osu_shiny.git
projects[osu_shiny][download][branch] = 7.x-1.x

projects[omega][type] = theme
projects[omega][version] = 4.0

projects[osu_omega][type] = theme
projects[osu_omega][download][type] = git
projects[osu_omega][download][url] = https://kautto@bitbucket.org/kautto/osu-omega.git
projects[osu_omega][download][branch] = 7.x-3.x

projects[osu_omega_extras][type] = module
projects[osu_omega_extras][subdir] = custom
projects[osu_omega_extras][download][type] = git
projects[osu_omega_extras][download][url] = https://kautto@bitbucket.org/kautto/osu-omega-extras.git
projects[osu_omega_extras][download][branch] = 7.x-3.x
